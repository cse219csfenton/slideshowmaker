package ssm.error;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.view.SlideShowMakerView;

/**
 * This class provides error messages to the user when the occur. Note
 * that error messages should be retrieved from language-dependent XML files
 * and should have custom messages that are different depending on
 * the type of error so as to be informative concerning what went wrong.
 * 
 * @author McKilla Gorilla & _____________
 */
public class ErrorHandler {
    // APP UI
    private SlideShowMakerView ui;
    
    // KEEP THE APP UI FOR LATER
    public ErrorHandler(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param errorType Identifies the type of error that happened, which
     * allows us to get and display different text for different errors.
     */
    public void processError(LanguagePropertyType errorType, LanguagePropertyType errorDialogTitle, String errorDialogMessage)
    {
        // GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String errorFeedbackText = props.getProperty(errorType);
             
        // POP OPEN A DIALOG TO DISPLAY TO THE USER
         Stage errorStage = new Stage();
        Button okBt = new Button("OK");
        Label errorLabel = new Label(props.getProperty(errorDialogTitle));
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(errorLabel, 10, 0);
        p.add(okBt, 10, 20);
        
        okBt.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            
            errorStage.close();
        }
        });
        
        //chnage icon
        errorStage.getIcons().add(new Image("ssmicon.png"));
        //primaryStage.getIcons().add(new Image("ssmicon.png"));
        
        
        Scene scene = new Scene(p, 450, 100);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        p.getStyleClass().add("btError");
        errorStage.setTitle(errorFeedbackText);
        errorStage.setScene(scene);
        
        
        errorStage.showAndWait();
        
        // @todo
    }    
}
