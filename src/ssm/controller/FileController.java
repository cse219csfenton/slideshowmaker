package ssm.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.LanguagePropertyType.YES;
import static ssm.LanguagePropertyType.NO;
import static ssm.LanguagePropertyType.CANCEL;
import static ssm.LanguagePropertyType.ERROR_DATA_FILE_LOADING;
import static ssm.LanguagePropertyType.SAVE;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.ERROR_DATA_FILE_SAVING;
import static ssm.LanguagePropertyType.NO_SAVE;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.PATH_SITES;
import ssm.controller.ImageSelectionController;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import static jdk.nashorn.internal.objects.NativeRegExp.source;
import static ssm.StartupConstants.STYLE_SHEET_UI;
/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & _____________
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;

    
    ImageView imageSelectionView;
    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();
		slideShow.reset(1);
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);

                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // @todo
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo provide error message
        }
        markAsEdited();
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            eH.processError(ERROR_DATA_FILE_LOADING, ERROR_DATA_FILE_LOADING, PATH_SLIDE_SHOWS);
            //@todo provide error message
        }
    }

    /**
     * This method will save the current slideshow to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
	    
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            eH.processError(ERROR_DATA_FILE_SAVING, NO_SAVE, PATH_SLIDE_SHOWS);
	    return false;
        }
    }
    int index = 0;
    public void copyFiles(File src, File dest) throws IOException{
        if(src.isDirectory()){
    		
    		//if directory not exists, create it
    		if(!dest.exists()){
    		   dest.mkdir();
    		   System.out.println("Directory copied from " 
                              + src + "  to " + dest);
    		}
    		
    		//list all the directory contents
    		String files[] = src.list();
    		
    		for (String file : files) {
    		   //construct the src and dest file structure
    		   File srcFile = new File(src, file);
    		   File destFile = new File(dest, file);
    		   //recursive copy
    		   copyFiles(srcFile,destFile);
    		}
    	   
    	}else{
    		//if file, then copy it
    		//Use bytes stream to support all file types
    		InputStream in = new FileInputStream(src);
    	        OutputStream out = new FileOutputStream(dest); 
    	                     
    	        byte[] buffer = new byte[1024];
    	    
    	        int length;
    	        //copy the file content in bytes 
    	        while ((length = in.read(buffer)) > 0){
    	    	   out.write(buffer, 0, length);
    	        }
 
    	        in.close();
    	        out.close();
    	        System.out.println("File copied from " + src + " to " + dest);
    	}
    }
    
    public void handleViewSlideShowRequest(SlideShowMakerView show){
        try{
        if (promptToSave()){
            handleSaveSlideShowRequest();
        }
        }catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            eH.processError(ERROR_DATA_FILE_LOADING, ERROR_DATA_FILE_LOADING, PATH_SLIDE_SHOWS);
            //@todo provide error message
        }
        String title = (show.getTitle());
        File slideDirectory = new File("./src/ssm/sites/" + SLASH+ title + SLASH + "slideShow.json");
        if (slideDirectory.exists()){
            slideDirectory.delete();
            System.out.println("zzzzzzzzzzzzzzzzzzzzzzzz");
        }
        else{
            System.out.println("sadljfhsad;ljfh");
        }
        new File("./src/ssm/sites/"+ SLASH+ title).mkdirs();
        new File("./src/ssm/sites/" + SLASH+ title +SLASH+ "img").mkdirs();
        new File("./src/ssm/sites/" + SLASH+ title +SLASH+ "css").mkdirs();
        new File("./src/ssm/sites/" + SLASH+ title +SLASH+ "js").mkdirs();
        new File("./src/ssm/sites/"+ SLASH+ title +SLASH+ "icons").mkdirs();
        SlideShowModel model = show.getSlideShow();
        ObservableList<Slide> slides = model.getSlides();
        ArrayList<String> slidePics = new ArrayList<String>();
        //copy over all the pictures
        for (Slide shower: slides){
            Path sourceIMG = Paths.get(shower.getImagePath() +  SLASH + shower.getImageFileName());
            Path destIMG = Paths.get("./src/ssm/sites/"+ SLASH+ title +SLASH+ "img");
            slidePics.add("./src/ssm/sites/" + SLASH+ title +SLASH+ "img" +SLASH+shower.getImageFileName());
            try {
                java.nio.file.Files.copy(sourceIMG, destIMG.resolve(shower.getImageFileName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //copy over json file, and rename it
        Path sourceJSON = Paths.get(PATH_SLIDE_SHOWS + SLASH + title + ".json");
        Path destJSON = Paths.get("./src/ssm/sites/" + SLASH+ title);
        try {
                java.nio.file.Files.copy(sourceJSON, destJSON.resolve("slideShow.json"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
        //copy over jquery
       
        Path sourceJQ = Paths.get("./JS/jquery-2.1.4.min.js");
        Path destJQ = Paths.get("./src/ssm/sites/" + SLASH+ title + SLASH + "js");
        try {
                java.nio.file.Files.copy(sourceJQ, destJQ.resolve("jquery-2.1.4.min.js"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
        
        //copy over the css
        Path sourceCSS = Paths.get("./HTML/public_html/ViewerCss.css");
        Path destCSS = Paths.get("./src/ssm/sites/" + SLASH+ title + SLASH + "css");
        try {
                java.nio.file.Files.copy(sourceCSS, destCSS.resolve("ViewerCss.css"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
         //copy over the js
        Path sourceJS = Paths.get("./JS/ssmjs.js");
        Path destJS = Paths.get("./src/ssm/sites/" + SLASH+ title + SLASH + "js");
        try {
                java.nio.file.Files.copy(sourceJS, destJS.resolve("ssmjs.js"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
         //copy over the HTML
        Path sourceHTML = Paths.get("./HTML/index.html");
        Path destHTML = Paths.get("./src/ssm/sites/" + SLASH+ title);
        try {
                java.nio.file.Files.copy(sourceHTML, destHTML.resolve("index.html"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
        
        //copy over the icons
        File sourceIcons = new File("./images/icons");
        File destIcons = new File("./src/ssm/sites/" + SLASH+ title + SLASH + "icons");
        try {
                copyFiles(sourceIcons, destIcons);
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
        Slide shower = slides.get(index);
        imageSelectionView = new ImageView();
        updateSlideImage(shower );
        
        
        //<script  src = "./js/jquery-2.1.4.min.js"> </script>
        //WebView browser = new WebView();
        //WebEngine webEngine = browser.getEngine();
        //webEngine.load("http://google.com");
        Browser b = new Browser();
        Stage stage = new Stage();
        Scene scene;
        stage.setTitle(show.getTitle());
        Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
        stage.setX(bounds.getMinX());
	stage.setY(bounds.getMinY());
	stage.setWidth(bounds.getWidth());
	stage.setHeight(bounds.getHeight());
        scene = new Scene(new Browser(),750,500, Color.web("#666970"));
        stage.setScene(scene);
        scene.getStylesheets().add("SlideShowMakerStyle.css");        
        stage.show();
    }
    
    
    
    
   public class Browser extends Region {
 
    final WebView browser = new WebView();
    
    final WebEngine webEngine = browser.getEngine();
     
    public Browser() {
        //apply the styles
        getStyleClass().add("tool_bar");
        // load the web page
        String path = getClass().getResource("/ssm/index.html").toExternalForm();
        webEngine.load(path);
        //add the web view to the scene
        getChildren().add(browser);
        webEngine.setJavaScriptEnabled(true);
    }
    private Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }
 
    @Override protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }
 
    @Override protected double computePrefWidth(double height) {
        return 750;
    }
 
    @Override protected double computePrefHeight(double width) {
        return 500;
    }
}
    
    
    public void updateSlideImage(Slide slide) {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
       
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
            ErrorHandler eH = ui.getErrorHandler();
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
            String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            eH.processError(ERROR_DATA_FILE_LOADING, ERROR_DATA_FILE_LOADING, PATH_SLIDE_SHOWS);
	}
    }    
    

     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
            String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            eH.processError(ERROR_DATA_FILE_LOADING, NO_SAVE, PATH_SLIDE_SHOWS);
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    boolean saveWork;
    boolean cancel =false;
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Stage saveStage = new Stage();
        Stage langStage = new Stage();
        Button btYES = new Button("       " + props.getProperty(YES.toString()) + "       ");
        Button btNO = new Button("       " +props.getProperty(NO.toString())+ "        ");
        Button btCANCEL = new Button("     " +props.getProperty(CANCEL.toString())+ "      " );
        Label langLabel = new Label(props.getProperty(SAVE.toString()));
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(langLabel, 10, 0);
        p.add(new Label(""), 10, 15);
        p.add(btYES, 10, 25);
         p.add(new Label(""), 10, 35);
        p.add(btNO, 10, 45);
         p.add(new Label(""), 10, 55);
        p.add(btCANCEL, 10, 65);
        
        p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 250);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        //btYES.getStyleClass().add("btYES");
        saveStage.setTitle("SAVE");
        saveStage.setScene(scene);
        
        
        ///if yes is selected
        btYES.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            saveWork = true;
            saveStage.close();
        }
        });
        
        
        ///if no is selected
        btNO.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            saveWork = false;
            saveStage.close();
        }
        });
        
        ///if cancel is selected
        btCANCEL.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            cancel = true;
            saveStage.close();
        }
        });
        
        
        saveStage.showAndWait();
        
        
        
        
        
         // @todo change this to prompt

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            SlideShowModel slideShow = ui.getSlideShow();
            slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        if (cancel) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                String path = selectedFile.getAbsolutePath();
                //title=title.substring(65, title.length()-5);
                int lastSlash =path.lastIndexOf("\\");
                String title = path.substring(lastSlash +1, path.lastIndexOf("."));
                
                ui.setTitle(title);
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
                
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                
            eH.processError(ERROR_DATA_FILE_LOADING, ERROR_DATA_FILE_LOADING, PATH_SLIDE_SHOWS);
                // @todo
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
}

