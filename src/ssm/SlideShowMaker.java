package ssm;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.LanguagePropertyType.NO_LANG;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & Chris Fenton
 */



public class SlideShowMaker extends Application {
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);
    String language = "x";
    @Override
    public void start(Stage primaryStage) throws Exception {
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        ObservableList<String> languages = FXCollections.observableArrayList("English", "Spanish");
        final ComboBox comboBox = new ComboBox(languages);
        //Stores language choice
        
        
        
        //create language window
        Stage langStage = new Stage();
        Button btEng = new Button("             English             ");
        Button btSpn = new Button("             Spanish             ");
        Button btok = new Button("    OK       ");
        btok.getStyleClass().add("lang_scene");
        Label langLabel = new Label("Choose a language");
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        //p.add(new Label("               "), 10, 0);
       // p.add(new Label("               "), 10, 10);
        //p.add(new Label("               "), 10, 20);
       // p.add(new Label("               "), 10, 30);
        p.add(langLabel, 20, 20);
        //p.add(btEng, 10, 25);
        //p.add(btSpn, 10, 45);
        p.getStyleClass().add("btYES");
        p.add(comboBox, 20, 30);
       
        p.add(btok, 20, 40);
        btok.setDisable(true);
        btok.setVisible(false);
        comboBox.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            btok.setDisable(false);
            btok.setVisible(true);
        }
        });
        
        
        btok.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            language = comboBox.getValue().toString();
            if (language.equalsIgnoreCase("English")){
                language = "e";
            }
            else{ language = "s";}
            langStage.close();
        }
        });
        
        //chnage icon
        langStage.getIcons().add(new Image("ssmicon.png"));
        primaryStage.getIcons().add(new Image("ssmicon.png"));
        
        
        Scene scene = new Scene(p, 350, 160);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        //scene.getStylesheets().add("lang_scene");
        langStage.setTitle("Language Selection");
        
        langStage.setScene(scene);
        
        
        ///if English is selected
        btEng.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            language = "e";
            langStage.close();
        }
        });
        
        ///If SApanish is selected
        btSpn.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            language = "s";
            langStage.close();
        }
        });
        
        langStage.showAndWait();
        
      
        
        
        if (language.equals("x")){
           System.exit(0);
        }
        
        boolean success = loadProperties(language);
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);

	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, appTitle);
	} // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
	else {
	    // LET THE ERROR HANDLER PROVIDE THE RESPONSE
	    ErrorHandler errorHandler = ui.getErrorHandler();
	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, NO_LANG, "todo");
	    System.exit(0);
	}
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties(String language) {
        if (language.equals("e")){
            try {
                // LOAD THE SETTINGS FOR STARTING THE APP
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
                props.loadProperties("properties_EN.xml", PROPERTIES_SCHEMA_FILE_NAME);
                return true;
           } catch (InvalidXMLFileFormatException ixmlffe) {
                // SOMETHING WENT WRONG INITIALIZING THE XML FILE
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, NO_LANG, "todo");
                return false;
            }        
        }
        else{
            try {
                // LOAD THE SETTINGS FOR STARTING THE APP
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
                props.loadProperties("properties_SP.xml", PROPERTIES_SCHEMA_FILE_NAME);
                return true;
           } catch (InvalidXMLFileFormatException ixmlffe) {
                // SOMETHING WENT WRONG INITIALIZING THE XML FILE
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, NO_LANG, "todo");
                return false;
        }
    }
    }
    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
        
	launch(args);
    }
}
