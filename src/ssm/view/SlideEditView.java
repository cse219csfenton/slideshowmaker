package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.view.SlideShowMakerView;
import ssm.model.SlideShowModel;


/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    
    //Slideshow Maker Viewto Contain list of slides
    SlideShowMakerView maker;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField = new TextField();
    String caption;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide, SlideShowMakerView ssmv) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	maker = ssmv;
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	slide.setEditor(this);
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
	//captionTextField = new TextField();
        
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);

	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
	    imageController.processSelectImage(slide, this);
            slide.setSelected(true);
            maker.getSlideShow().selected(slide);
            slide.setSelected(true);
            maker.getSlideShow();
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
	});
        
        captionVBox.setOnMousePressed(e -> {
	    //imageController.processSelectImage(slide, this);
            slide.setSelected(true);
            maker.getSlideShow().selected(slide);
            slide.setSelected(true);
            maker.getSlideShow();
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
	});
        captionTextField.setOnKeyPressed(e ->{
            ssmv.updateToolbarControls(false);
            
        });
        ssmv.gettf().setOnKeyPressed(e ->{
            ssmv.updateToolbarControls(false);
            
        });
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
        if (slide.getCaption() != null){  setCaption(slide.getCaption()); }
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    // @todo - use Error handler to respond to missing image
	}
    }    
    
    public String getTextField(){
         if (captionTextField != null){
            return captionTextField.getText();
        }
        else return "";
    }
    
    
    public String getCaption(){
        if (captionTextField.getText() != null){
            return captionTextField.getText();
        }
        else return "";
        
    }
    
    public void setSlideCaption(){
        slide.setCaption(captionTextField.getText());
    }
    
    public void setCaption(String cap){
        captionTextField.setText(cap);
        
    }
    
    public SlideShowMakerView getMaker(){
        return maker;
    }
}